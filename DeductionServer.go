package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type postmanRequest struct {
	SellerID string `json:"seller_id"`
	DeductionAmount float64 `json:"deduction_amount"`
	NumberOfDeduction int `json:"number_of_deduction"`
	DateOfDeduction string `json:"date_of_deduction"`
}

func deductionRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var request postmanRequest
	_ = json.NewDecoder(r.Body).Decode(&request)
	json.NewEncoder(w).Encode(request)
}

func main() {

	db, err := sql.Open("mysql", "root:Lqs@1995(127.0.0.1:3306)/EntryTask")
	if err != nil {
		log.Fatal("Unable to connect to db")
	}else {
		fmt.Println("Connected to db")
	}
	defer db.Close()

	results, err := db.Query("Select seller_id FROM seller")
	if err != nil {
		log.Fatal("Error fetching from seller table ", err)
	}else {
		fmt.Println("Fetched")
	}

	defer results.Close()

	for results.Next() {
		var (
			seller_id int
			seller_name string
		)
		err = results.Scan(&seller_id, &seller_name)
		if err != nil {
			log.Fatal("Unable to parse row", err)
		}
		fmt.Printf("ID: '%s', Price: %s\n", seller_id, seller_name)
	}

	router := mux.NewRouter()

	router.HandleFunc("/deductionServer", deductionRequest).Methods("POST")

	log.Fatal(http.ListenAndServe(":8080", router))

}